import os
import json
import time
import signal
import tornado.ioloop
import tornado.template
import tornado.httpclient

def color(string, color):
	escape = '\033[0m'
	color_code = ''
	if color == 'g':
		color_code = '\033[0;32m'
	if color == 'r':
		color_code = '\033[0;31m'
	if color == 'w':
		color_code = '\033[0;33m'
	if color == 'i':
		color_code = '\033[0;34m'
	return '{}{}{}'.format(color_code, string, escape)

def log(m_type, message, color_code):
	print( '{time} [ {type} ] {message}'.format( time=time.strftime('%Y-%m-%d %H:%M:%S'), type=color(m_type, color_code), message=message ) )

def signal_handler(signal, frame):
	log('STOP', 'shutdown', 'r')
	tornado.ioloop.IOLoop.instance().stop()



services = {}
def main():
	print('--------------------------------------------')
	log('START', 'reconfigure daemon started', 'g')
	listen()
	signal.signal(signal.SIGTERM, signal_handler)
	signal.signal(signal.SIGINT, signal_handler)
	tornado.ioloop.IOLoop.instance().start()

def handle_stream(chunk):
	global services
	data = json.loads(chunk.decode('utf-8'))
	if data['action'] == 'set':
		addInstance( data )
	if data['action'] == 'expire' or data['action'] == 'delete':
		delInstance( data )

def handle_response(response):
	if response.error:
		if response.error.code == 599:
			log('WARN', 'HTTP timeout while listening', 'w')
		else:
			log('ERR', 'HTTP error while listening {}:{}'.format(response.error.code, response.error.response), 'r')
	else:
		log('ERR', 'event stream closed unexpectedly', 'r')
	listen()

def listen():
	client = tornado.httpclient.AsyncHTTPClient()
	request = tornado.httpclient.HTTPRequest( url='http://localhost:2379/v2/keys/services?stream=true&wait=true&recursive=true', streaming_callback=handle_stream, request_timeout=3600.0 )
	client.fetch( request, handle_response )
	log('INFO', 'listening to stream', 'i')



def addInstance(data):
	global services
	key = data['node']['key']
	tmp = key.split('@')
	instance_id = tmp[1]
	service_name = tmp[0][10:] # cut /services/ from key

	if not service_name in services:
		addService(service_name)
	if instance_id in services[service_name]:
		return # already registered

	try:
		services[service_name][instance_id] = json.loads(data['node']['value']) # add new instance
	except ValueError:
		log('ERR', 'malformed json at {}@{}'.format(service_name, instance_id), 'r')
		return
	log('ADD', 'instance {}@{} added'.format(service_name, instance_id), 'g')
	reconfigureNginx()

def delInstance(data):
	global services
	key = data['node']['key']
	tmp = key.split('@')
	instance_id = tmp[1]
	service_name = tmp[0][10:] # cut /services/ from key

	services[service_name].pop(instance_id, None)
	if len(services[service_name]) == 0:
		delService(service_name)
	log('DEL', 'removed instance {}@{}'.format(service_name, instance_id), 'w')
	reconfigureNginx()

def addService(name):
	global services
	services[name] = {}
	log('ADD', 'service {} added'.format(name), 'g')

def delService(name):
	global services
	services.pop(name, None)
	log('DEL', 'removed service {}'.format(name), 'w')

def reconfigureNginx():
	global services
	loader = tornado.template.Loader('/root')
	config_file = open('/etc/nginx/nginx.conf', 'w')
	config_file.truncate()
	config_file.write( loader.load('nginx.config.template').generate(services=services).decode('utf-8') )
	config_file.close()
	os.system('/usr/sbin/service nginx reload')
	log('INFO', 'nginx updated', 'i')



if __name__ == '__main__':
	main()
