#!/bin/sh

print() {
	reset='\033[0m'

	case $2 in
		info)
			color='\033[0;34m';;
		success)
			color='\033[0;32m';;
		error)
			color='\033[0;31m';;
		warning)
			color='\033[0;33m';;
	esac

	echo -e "${color}${1}${reset}"
}
error() {
	print "$1" error
	exit 1
}

install_binaries() {
	echo "  Installing binaries"

	# docker
	echo "  -> installing docker ..."
	yum install -y docker
	if [ $? -ne 0 ]; then
		error "Error while installing docker"
	fi
	print "     [ok]" success
	echo

	# etcd
	echo "  -> installing etcd ..."
	wget -q https://github.com/coreos/etcd/releases/download/v2.0.10/etcd-v2.0.10-linux-amd64.tar.gz
	tar -xzf etcd-v2.0.10-linux-amd64.tar.gz
	mv ./etcd-v2.0.10-linux-amd64/etcd /usr/local/bin/
	mv ./etcd-v2.0.10-linux-amd64/etcdctl /usr/local/bin/
	rm etcd-v2.0.10-linux-amd64.tar.gz
	rm -rf etcd-v2.0.10-linux-amd64
	if [ $? -ne 0 ]; then
		error "     [error]"
	fi
	print "     [ok]" success
	echo

	# fleet
	echo "  -> installing fleet ..."
	wget -q https://github.com/coreos/fleet/releases/download/v0.10.0/fleet-v0.10.0-linux-amd64.tar.gz
	tar -xzf fleet-v0.10.0-linux-amd64.tar.gz
	mv ./fleet-v0.10.0-linux-amd64/fleetd /usr/local/bin/
	mv ./fleet-v0.10.0-linux-amd64/fleetctl /usr/local/bin/
	rm fleet-v0.10.0-linux-amd64.tar.gz
	rm -rf fleet-v0.10.0-linux-amd64
	if [ $? -ne 0 ]; then
		error "     [error]"
	fi
	print "     [ok]" success
	echo

	# flanneld
	echo "  -> installing flanneld ..."
	wget -q https://github.com/coreos/flannel/releases/download/v0.3.1/flannel-0.3.1-linux-amd64.tar.gz
	tar -xzf flannel-0.3.1-linux-amd64.tar.gz
	mv ./flannel-0.3.1/flanneld /usr/local/bin/
	rm flannel-0.3.1-linux-amd64.tar.gz
	rm -rf flannel-0.3.1
	if [ $? -ne 0 ]; then
		error "     [error]"
	fi
	print "     [ok]" success
	echo
}

install_services() {
	echo "  Installing systemd services"
	systemctl stop docker
	systemctl stop etcd
	systemctl stop flanneld
	systemctl stop fleet

	#etcd
	read -p "  Enter domain for service discovery: " DOMAIN
	read -p "  Enter fleet metadata (disk=hdd,provider=df,platform=xen): " METADATA
	sed -E -i 's/##DOMAIN##/'$DOMAIN'/' etcd.service
	sed -E -i 's/##IP##/'$(ifconfig eth0 | sed -nE 's/.*inet ([[:digit:]|\.]*).*/\1/p')'/' etcd.service
	mv ./etcd.service /lib/systemd/system/

	#fleet
	sed -E -i 's/##METADATA##/'$METADATA'/' fleet.service
	mv -f ./fleet.service /lib/systemd/system/
	mv -f ./fleet.socket /lib/systemd/system/

	#flanneld
	mv -f ./flanneld.service /lib/systemd/system/

	#docker
	mv -f ./docker.service /lib/systemd/system/

	#systemd
	systemctl daemon-reload
	echo 'systemctl start etcd flanneld fleet'
	echo 'etcdctl set /coreos.com/network/config '\''{ "Network": "10.1.0.0/16" }'\'
	echo 'systemctl start docker'
}

install_loadbalacer() {
	echo "  Building loadbalancer"
	cd loadbalancer
	docker build -t loadbalancer .
	echo "  -> starting loadbalancer..."
	docker run -d --name loadbalancer --net="host" loadbalancer
	echo "  -> starting watcher for new services..."
	docker exec loadbalancer python3 watcher.py
	cd ..
}

# start
echo "---------------------------------------------------"
print "  Cluster - Installer" info
if type -P docker &> /dev/null; then
    echo -e "  - docker    \033[0;32mINSTALLED\033[0m ("$(docker --version)")"
else
    echo -e "  - docker    \033[0;31mNOT INSTALLED\033[0m"
fi
if type -P etcd &> /dev/null; then
    echo -e "  - etcd      \033[0;32mINSTALLED\033[0m ("$(etcd --version)")"
else
    echo -e "  - etcd      \033[0;31mNOT INSTALLED\033[0m"
fi
if type -P fleetd &> /dev/null; then
    echo -e "  - fleet     \033[0;32mINSTALLED\033[0m ("$(fleetd --version)")"
else
    echo -e "  - fleet     \033[0;31mNOT INSTALLED\033[0m"
fi
if type -P flanneld &> /dev/null; then
    echo -e "  - flannel   \033[0;32mINSTALLED\033[0m"
else
    echo -e "  - flannel   \033[0;31mNOT INSTALLED\033[0m"
fi
echo

# install binaries
while true; do
    read -p "  Install binaries (docker, etcd, fleet, flannel)? yes/no " yn
    case $yn in
        [Yy]* ) install_binaries; break;;
        [Nn]* ) break;;
        * ) print "  Please answer yes or no." warning;;
    esac
done

# install services
echo "---------------------------------------------------"
while true; do
    read -p "  Install services? yes/no " yn
    case $yn in
        [Yy]* ) install_services; break;;
        [Nn]* ) break;;
        * ) print "  Please answer yes or no." warning;;
    esac
done

# build loadbalancer
echo "---------------------------------------------------"
while true; do
    read -p "  Build loadbalancer? yes/no " yn
    case $yn in
        [Yy]* ) install_services; break;;
        [Nn]* ) break;;
        * ) print "  Please answer yes or no." warning;;
    esac
done